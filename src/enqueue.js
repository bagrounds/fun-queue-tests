;(function () {
  'use strict'

  /* imports */
  var funTest = require('fun-test')
  var verifiers = require('fun-test-verifiers')

  /* exports */
  module.exports = test()

  function test () {
    var testItem = 'this is a test item'

    var testInput = {
      item: testItem
    }

    var verifier = verifiers.output.equalStrict(testItem)

    var funTestOptions = {
      input: testInput,
      verifier: verifier,
      transformer: transformer
    }

    var test = funTest(funTestOptions)

    test.description = 'enqueues items successfully'

    return test
  }

  function transformer (funQueue) {
    var TEST_ID = 'test id'

    return function (options, callback) {
      var testItem = options.item

      funQueue.create(TEST_ID, handleCreate)
      
      function handleCreate (error, id) {
        if (error) {
          callback(error)
          return
        }

        var enqueueOptions = {
          id: TEST_ID,
          item: testItem
        }

        funQueue.enqueue(enqueueOptions, handleEnqueue)
      }

      function handleEnqueue (error, success) {
        if (error) {
          callback(error)
          return
        }

        if (!success) {
          callback(new Error('enqueue() did not return success response'))
          return
        }

        var dequeueOptions = {
          id: TEST_ID
        }

        funQueue.dequeue(dequeueOptions, callback)
      }
    }
  }
})()

