;(function () {
  'use strict'

  /* imports */
  var funTest = require('fun-test')
  var verifiers = require('fun-test-verifiers')

  /* exports */
  module.exports = test()

  function test () {
    var testId = 'test id'

    var testInput = {
      id: testId
    }

    var verifier = verifiers.output.equalStrict(testId)

    var funTestOptions = {
      input: testInput,
      verifier: verifier,
      transformer: transformer
    }

    var test = funTest(funTestOptions)

    test.description = "create(id) returns id"

    return test
  }

  function transformer (funQueue) {
    return function (options, callback) {
      var id = options.id

      funQueue.create(id, callback)
    }
  }
})()

